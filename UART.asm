get_char:
	li $t7, 0x2
	lw $t0, 4($s0)	#load status register
	and $t0, $t0, $t7	#mask for ready bit
	bne $t0, $t7, get_char
	nop
	lw $s6, 8($s0)	#load from recieve buffer
	sw $t7, 0($s0)	#command register: clear status
	jr $ra
	nop
