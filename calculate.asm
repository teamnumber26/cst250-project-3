# Performs mathematical operations on the fly
init_calc:
	li $t1, 1
	li $t2, 2
	li $t3, 3	
	beq $s7, $t1, is_num
	nop
	beq $s7, $t2, is_op
	nop
	beq $s7, $t3, is_equal
	nop


is_num:
	# if prev char was also a number
	li $t3, 1
	beq $s5, $t3, number_append
	nop
	
	# prev is operator then set_s4
	li $t3, 2
#	beq $s5, $t3, set_s4
	beq $s5, $t3, check_is_s2_empty
	nop

	j set_s2
	nop

check_is_s2_empty:
	li $t5, -1
	beq $s2, $t5, set_s2
	nop
	j set_s4
	nop

number_append:
	# if s4 is null, then append to s2 else append to s4
	li $t3, -1
	beq $s4, $t3, string_to_value_s2
	nop
	j string_to_value_s4
	nop

is_op:
	# if prevdigit was a operator and this is negative then set 2's complement flag = true
	li $t3, 2
	beq $s5, $t3, prev_op
	nop

	# if prev char was a number then check and calculate
	li $t3, 1
	beq $s5, $t3, set_temp_op
	nop
	
	li $v0, 1
	j set_flag
	nop
	

prev_op:
	li $t4, 45
	beq $s6, $t4, set_flag
	nop
	
set_flag:
	li $s1, 1
	j jump_back
	nop

set_temp_op:
	li $t9, -1
	j calculate
	nop

is_equal:	
	j check_negative_flag
	nop


# Check if 1 operator and 2 operands are present and then calculate
calculate:
	li $t0, -1
	beq $s3, $t0, set_s3 # $s3 not present
	nop

	j check_negative_flag
	nop

perform_operations:
	# We can Calculate!
	li $t0, 43
	beq $s3, $t0, add
	nop

	li $t0, 45
	beq $s3, $t0, sub
	nop

	li $t0, 42
	beq $s3, $t0, mul
	nop

	li $a0, 0
	j jump_back
	nop

check_negative_flag:

	li $t4, 1
	beq $s1, $t4, s2_or_s4
	nop
	j perform_operations
	nop

s2_or_s4:
	li $t5, 1
	beq $v0, $t5, unsigned_to_negative_s2
	nop
	j unsigned_to_negative
	nop

add:
	addu $s2, $s2, $s4
	j reset
	nop

sub:
	subu $s2, $s2, $s4
	j reset
	nop

mul:
	mullo $s2, $s2, $s4
	j reset
	nop	

# Reset constants
reset:
	move $a0, $s2
	li $s3, -1
	li $s1, 0
	li $s4, -1
	li $t8, -1
	li $a2, 0
	beq $t9, $t8, set_s3
	nop
	jr $ra
	nop

set_s4:
	move $s4, $s6
	j jump_back
	nop

set_s2:
	move $s2, $s6
	j jump_back
	nop

set_s3:
	li $t9, 0
	move $s3, $s6
	j jump_back
	nop

jump_back:
	jr $ra
	nop