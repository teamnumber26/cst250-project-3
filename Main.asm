# main source file
.org 0x10000000

#Stack Pointer   
li $sp, 0x10fffffc

restart:
# Calculation Constants (used in calculate.asm)
li $s4, -1
li $s3, -1
li $s2, -1
li $a0, 0
li $a1, 0
li $s1, 0

#UART Functions
li $s0, 0xF0000000	#UART


#Calculator Values
li $t5, 0 	#sign bit
li $s6, 0	#current number

#previous charecter
li $s7, 0 #used to store current type
li $s5, 0  #used to store previous type
li $a1, 0
#li $s2,0
get_num:
	li $t1, 0		#x: used for UART character reads and writes
	move $s5,$s7
	jal get_char
	nop
	
	bne $a1, $0 Thereisanerror
	nop
	
	#move $s6, $t1
	#nop

	jal is_operator
	nop
	jal is_number
	nop
	
	j get_num
	nop