# New ASM File
#Description: checks what opperator was entered
is_operator:
	#check if operator is character is '='
	li $t2, 61
	beq $s6, $t2, is_equals
	nop
	li $t0, 42
	beq $t0, $s6 op
	nop
	addiu $t0,$t0 1
	beq $t0, $s6 op
	nop
	addiu $t0,$t0 2
	beq $t0, $s6 op
	nop
	jr $ra
	nop

#Description: Calls compute_result and then calls display_result
is_equals:
	li $s7, 3
	j error_checking
	nop
	
op:
	li $s7,2
	j error_checking
	nop
	

#Description: Checks if $t1 is ascii decimal number
#Resources: $t0 = temp
is_number:

	li $t0, 0x30	#ascii 0
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 1
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 2
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 3
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 4
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 5
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 6
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 7
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 8
	beq $t0, $s6, valid_input
	nop
	addiu $t0, $t0, 1	#acscii 9
	beq $t0, $s6, valid_input
	nop
	j invalid_input
	nop

valid_input:
	#push $ra
	li $s7,1	
	li $t4, 48
	subu $s6, $s6, $t4	#convert x from ascii to decimal
	j error_checking
#	jal string_to_value
	#nop
	#pop $ra
	#jr $ra
	nop