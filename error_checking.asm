error_checking:

li $t1, 1
li $t2, 2
li $t3, 3
li $t4, 61 # equal to operator
li $t5, 45 # negative operator
li $t6, 0

bne $s5, $zero, check_for_number
	nop
	beq $s7, $t3, done
	nop
	bne $s7, $t2, done
		nop
		beq $s6, $t5, done  # verifying if current character is negative sign
		nop
		li $a1, 2
		j Thereisanerror
		nop	 

check_for_number:
bne $s5, $t1, check_for_operator
	nop
	j done # jump to append
	nop

check_for_operator:
bne $s5, $t2, check_for_equalto
	beq $s7, $t1, done
	nop
	beq $s6, $t5, flag_negative_sign_after_operator  # verifying if current character is negative sign
	nop		
	li $a1, 2
	j done
	nop
	
	flag_negative_sign_after_operator:
	addiu $a2, $a2, 1  # occurence of negative sign after an operator
	slt $t6, $t1, $a2 # checking if more than one such occurence in a sub-expression
	
	beq $t6, $t1, check_for_others   
	nop
	
	j done
	nop

check_for_equalto:
bne $s5, $t3, check_for_others
	nop
	beq $s7, $t1, done
	nop
	beq $s6, $t5, done  # verifying if current character is negative sign
	nop
	beq $s6, $t4, done # verifying if current character is equal to
	nop
	li $a1, 2
	j done
	nop

check_for_others:
li $a1, 2

done:
bne $a1, $zero, Thereisanerror 
nop
push $ra
jal init_calc
nop
pop $ra
# if equal_to call proj3_output_number
#j get_num
nop

li $t3, 3
beq $s7, $t3, print_output
nop

j get_num
nop 

print_output:
call project3_output_number
j restart
nop