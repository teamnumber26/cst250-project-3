unsigned_to_negative:
	li $t6, 0x1
	nor $s4, $s4, $s4	#NOT result
	addu $s4, $s4, $t6	#add 1 to result
	# reset 2s complement flag
	j perform_operations
	nop

unsigned_to_negative_s2:
	li $v0, 0
	li $t6, 0x1
	nor $s2, $s2, $s2	#NOT result
	addu $s2, $s2, $t6	#add 1 to result
	# reset 2s complement flag
	j perform_operations
	nop

string_to_value_s2:
	li $t3, 10	
	li $t6, 0x1
	mulhi $t9, $s2, $t3
	bne $t9, $0, input_overflow	#result has caused an overflow
	nop
	mullo $s2, $s2, $t3	#multiply current result by 10
	srl $t9, $s2, 31 	#get sign bit
	beq $t6, $t9, input_overflow	#sign bit has been set, invalid_input
	nop
	
	addu $s2, $s2, $s6	#current number + x
	srl $t9, $s2, 31 	#get sign bit
	beq $t6, $t9, input_overflow	#sign bit has been set, invalid_input
	nop
	jr $ra

string_to_value_s4:
	li $t3, 10	
	li $t6, 0x1
	mulhi $t9, $s4, $t3
	bne $t9, $0, input_overflow	#result has caused an overflow
	nop
	mullo $s4, $s4, $t3	#multiply current result by 10
	srl $t9, $s2, 31 	#get sign bit
	beq $t6, $t9, input_overflow	#sign bit has been set, invalid_input
	nop
	addu $s4, $s4, $s6	#current number + x
	srl $t9, $s4, 31 	#get sign bit
	beq $t6, $t9, input_overflow	#sign bit has been set, invalid_input
	nop
	jr $ra